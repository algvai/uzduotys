#Užduotis: https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=602&page=show_problem&problem=4434
#Lukas Knašas IFF-3/1

import random
import sys
import os

class Lagaminas:
    duom = ""
    rez = ""
    array = []

    def __init__(self, duom, rez):
        self.duom = duom
        self.rez = rez

    def readfile(self):
        with open(self.duom) as rf:
            n = rf.readline()
            for n in rf:
                self.array.append([float(x) for x in n.split()])
        rf.close()
        return;

    def artelpa(self):
        m = 0
        wf = open(self.rez, "w")
        for i in range(len(self.array)):
            if self.array[i][0] < 56.01:
                if self.array[i][1] < 45.01:
                    if self.array[i][2] < 25.01:
                        if self.array[i][3] < 7.01:
                            print("1")
                            wf.write("1\n")
                            m += 1
            elif ((self.array[i][0] + self.array[i][1] + self.array[i][2]) < 125.01) and (self.array[i][3] < 7.01):
                print("1")
                wf.write("1\n")
                m += 1
            else:
                print("0")
        print(m)
        wf.write(str(m))
        wf.close()


L = Lagaminas("duom.txt", "rez.txt")
L.readfile()
L.artelpa()


